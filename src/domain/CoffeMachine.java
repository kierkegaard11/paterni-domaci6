/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author maja
 */
public class CoffeMachine {

    public static final String CURRENCY_100 = "100";
    public static final String CURRENCY_50 = "50";
    public static final String CURRENCY_20 = "20";
    public static final String CURRENCY_1 = "1";
    
    private Map<String, Currency> currencies;
    
    public CoffeMachine() {
        currencies = new HashMap<>();
        currencies.put(CURRENCY_100, new Currency(1));
        currencies.put(CURRENCY_50, new Currency(0));
        currencies.put(CURRENCY_20, new Currency(1));
        currencies.put(CURRENCY_1, new Currency(0));
    }
    
    public int get(String key) {
        return currencies.get(key).getAmount();
    }
    
    public void set(String key, int amount) {
        currencies.get(key).setAmount(amount);
    }
    
    public Map<String, Currency> getCurrencies() {
        return currencies;
    }
    
}

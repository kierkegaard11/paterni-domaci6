/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.listeners;

import controller.Controller;
import domain.User;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author maja
 */
public class BtnLoginActionListener implements ActionListener {

    private Controller controller;
    
    public BtnLoginActionListener() {
        controller = Controller.getInstance();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String username = controller.getFrmLogin().getTxtUsername().getText().trim();
        String password = new String(controller.getFrmLogin().getTxtPassword().getPassword());
        try {
            login(username, password);
            controller.getFrmLogin().dispose();
            controller.getFrmSettings().setVisible(true);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(controller.getFrmLogin(), ex.getMessage());
        }
    }

    private User login(String username, String password) throws Exception {
        List<User> users = controller.getAllUsers();
        for (User u : users) {
            if (u.getUsername().equals(username)) {
                if (u.getPassword().equals(password)) {
                    return u;
                } else {
                    throw new Exception("Wrong password!");
                }
            }
        }
        throw new Exception("You aren't registered!");
    }
}

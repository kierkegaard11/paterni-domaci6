/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.listeners;

import controller.Controller;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author maja
 */
public class TxtDocumentListener implements DocumentListener {

    @Override
    public void insertUpdate(DocumentEvent e) {
        setReturns();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        setReturns();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    private void setReturns() throws NumberFormatException {
        Controller controller = Controller.getInstance();
        
        JTextField txtReturns = controller.getFrmMain().getTxtReturns();
        if (controller.isNoAmountProvided()) {
            txtReturns.setText(0 + "");
        } else {
            int price;
            try {
                price = Integer.parseInt(controller.getFrmMain().getTxtPrice().getText());
            } catch (NumberFormatException ex) {
                price = 0;
            }
            int providedMoney = controller.getProvidedMoney();

            int change = providedMoney - price;
            if (change >= 0) {
                txtReturns.setText(change + "");
            } else {
                txtReturns.setText(0 + "");
            }
        }
    }

}

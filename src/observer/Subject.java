/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author maja
 */
public class Subject {

    private static Subject instance;
    private List<Observer> observers;
    
    private Subject() {
        observers = new ArrayList<>();
    }
    
    public static final Subject getInstance() {
        if (instance == null) {
            instance = new Subject();
        }
        return instance;
    }
    
    public void subscribe(Observer observer) {
        observers.add(observer);
    }
    
    public void notifyAllObservers() {
        for (Observer observer : observers) {
            observer.notifyObserver();
        }
    }
    
}

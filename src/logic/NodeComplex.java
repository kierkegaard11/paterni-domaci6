/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import domain.Currency;
import java.util.Map;

/**
 *
 * @author maja
 */
public class NodeComplex extends Node {

    public NodeComplex(int currencyAmount, String currencyKey) {
        super(currencyAmount, currencyKey);
    }

    @Override
    public void getCurrency(Currency currency, Map<String, Integer> map) {
        if (controller.getCoffeMachine().get(currencyKey) > 0 && currency.getAmount() >= 0) {
            int amount = currency.getAmount() / currencyAmount;
            int remainder = currency.getAmount() % currencyAmount;
            map.put(currencyKey, amount);
            if (remainder != 0) {
                next.getCurrency(new Currency(remainder), map);
            }
        } else {
            map.put(currencyKey, 0);
            next.getCurrency(currency, map);
        }
    }
    
}

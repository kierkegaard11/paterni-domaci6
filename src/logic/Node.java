/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import controller.Controller;
import domain.Currency;
import java.util.Map;

/**
 *
 * @author maja
 */
public abstract class Node {

    protected Controller controller;
    protected int currencyAmount;
    protected String currencyKey;
    protected Node next;

    public Node(int currencyAmount, String currencyKey) {
        this.controller = Controller.getInstance();
        this.currencyAmount = currencyAmount;
        this.currencyKey = currencyKey;
    }
    
    public void setNext(Node cvor) {
        next = cvor;
    }

    public abstract void getCurrency(Currency currency, Map<String, Integer> map);
    
}
